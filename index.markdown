---
title: Hello
description: Colin is software engineer in the UK. He likes building things.
---

I'm Colin. I work with technology professionally, but I also like building, reading, and learning things for fun too.

I mainly write about software-related things on this site. I like scalable and resilient systems that can run fast, so I find the languages and the software that enable that behaviour fascinating. I occasionally write about other aspects of my life on this site too. Hopefully you'll find it all entertaining!

I work freelance. If you'd like to hire me, check out <a href="http://kickstandconsulting.co.uk/">Kickstand Consulting</a>.

You can also find me on
 <a href="https://www.instagram.com/cj_webb">Instagram</a>,
 <a href="https://twitter.com/colinjwebb">Twitter</a>,
 <a href="https://github.com/cjwebb">Github</a>,
 <a href="https://gitlab.com/cjwebb">Gitlab</a>, and
 <a href="https://www.linkedin.com/in/colinjwebb/">LinkedIn</a>.


## Writing

$partial("templates/post-list.html")$

