# My Blog

This blog is statically generated using [Hakyll](https://jaspervdj.be/hakyll/).

Assuming that you already have Haskell's Stack installed, you can run this locally by running these four commands after cloning.

    stack setup
    stack install
    stack exec site build
    stack exec site watch

## Todo

- Make dates display with '1st' instead of 01?
- Wrap post block in link, so can click the subtext too.
- [Latex equations](https://github.com/liamoc/latex-formulae)
- Commenting
- User metrics?
- Labels

### Date suffixes:
1, 21, 31 -> st
2, 22, 32 -> nd
3, 23, 33 -> rd
_ -> th

## Blog Ideas
- Basic electronic circuits with a 3 year old.
- Revisiting Javascript bind / prototypes / 'this'
- Fast MRI, and [why it is okay to fake MRI scans](https://engineering.fb.com/ai-research/fastmri/)? I've probably misunderstood this, but its an opportunity for learning interesting stuff.

