--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import qualified Data.ByteString.Lazy.Char8 as C
import           Text.Jasmine
import           System.FilePath.Posix (dropExtension,takeBaseName,takeFileName,takeDirectory,(</>))
import           Data.List (isSuffixOf)

--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
    match "images/**" $ do
        route   idRoute
        compile copyFileCompiler

    -- merge css assets together, and compress
    -- https://groups.google.com/forum/#!topic/hakyll/AIkHo1uyZoY
    match "css/*" $ compile getResourceBody
    create ["master.css"] $ do
        route   idRoute
        compile $ do
          items <- loadAll "css/*"
          makeItem $ compressCss $ concatMap itemBody (items :: [Item String])

    match "js/**" $ do
        route   idRoute
        compile compressJsCompiler

    match (fromList ["404.markdown"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "posts/*" $ do
        route $ prettyRoute
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match "index.markdown" $ do
        route $ setExtension "html"
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= renderPandoc
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= cleanUrls

    match "templates/*" $ compile templateCompiler

    create ["atom.xml"] $ do
      route idRoute
      compile $ do
          let feedCtx = postCtx `mappend` bodyField "description"
          posts <- fmap (take 10) . recentFirst =<<
              loadAllSnapshots "posts/*" "content"
          renderAtom myFeedConfiguration feedCtx posts

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%Y, %B %d" `mappend`
    defaultContext

compressJsCompiler :: Compiler (Item String)
compressJsCompiler = do
  let minifyJS = C.unpack . minify . C.pack . itemBody
  s <- getResourceString
  return $ itemSetBody (minifyJS s) s

myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
    { feedTitle       = "cjwebb.com"
    , feedDescription = "Blog of Colin Webb, a software consultant in the UK"
    , feedAuthorName  = "Colin Webb"
    , feedAuthorEmail = "hello@cjwebb.com"
    , feedRoot        = "https://cjwebb.com"
    }

cleanUrls :: Item String -> Compiler (Item String)
cleanUrls = return . fmap (withUrls clean)
  where
    clean url = case takeFileName url of
      "index.html" -> takeDirectory url
      _            -> url

prettyRoute :: Routes
prettyRoute = customRoute $ \identifier ->
  let filePath   = toFilePath identifier
      directory  = takeDirectory filePath
      baseName   = takeBaseName filePath
      simpleName = dropExtension (dropDate baseName)
  in  directory </> simpleName </> "index.html"

dropComponent :: String -> String
dropComponent str = case break (== '-') str of
  (_, _: str') -> str'
  (str', [])   -> str'

dropDate :: String -> String
dropDate = dropComponent . dropComponent . dropComponent

